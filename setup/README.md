# Contenuto della cartella */setup*

1. easy50.txt : 50 facili schemi che si possono importare nel gioco (quelli contrassegnati da * sono già stati importati)
1. sudoku.db : database iniziale con alcuni schemi precaricati (nessuna partita salvata)
1. sudoku-dbinit.sql : istruzioni sql per creare il db partendo da uno vuoto (inutile se si usa *sudoku.db* )
