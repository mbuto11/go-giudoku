package main

import (
    "fmt"
    "time"
    "net/http"
    "strings"
    "log"
    "database/sql"
    _ "github.com/mattn/go-sqlite3"
)

const ver = "local-3.10-w3"
const chglog = `
3.10: Completamento Caricam. Schemi<br>
3.9: Ordina partite dalle pi&ugrave; recenti<br>
3.8: Expert mode size<br>
3.7: Orange<br>
3.6: Compact View<br>
3.4: Import schema<br>
3.3: Porting local (da appengine)<br>
3.2: Aggiustamenti cosmetici<br>
3.1: Colore su Posizione impossibile<br>
3.0: Messaggio Correggi gli errori<br>
3.0: Messaggio Posizione impossibile<br>
`

var globaldb *sql.DB
var Disabdata string

// per il sort
type Sugge struct {
Numsugg int
Kappa int
Ottim string
}

type Suggerimenti [82]Sugge

// fine sort

func miosort(arr Suggerimenti) Suggerimenti {

for uu:=0; uu < 7000; uu++ {
esci := true
for k:=0; k < len(arr); k++ {
for j:=k+1; j < len(arr); j++ {
if arr[k].Numsugg > arr[j].Numsugg {
	tmp := arr[k]
	arr[k] = arr[j]
	arr[j] = tmp
	esci = false
} // if
} // j
} // k
if esci { return arr }
} //loop
return arr
} // miosort

func init() {
    http.HandleFunc("/", root)
    http.HandleFunc("/help", help)
    http.HandleFunc("/gotdata", got)
    http.HandleFunc("/listaschemi", listaschemi)
    http.HandleFunc("/listapartite", listapartite)
    http.HandleFunc("/vers", vers)
    http.HandleFunc("/isch", isch)
database, err1 := sql.Open("sqlite3", "sudoku.db")
if err1 != nil {
	log.Fatal("err open db")
	return
}
globaldb = database
}

type byte82 [82]byte
var arraydata byte82
var errordata byte82

type Schema struct {
Vd string
}

type Partita struct {
Vd string
Ds string
Oo string
}


func root(w http.ResponseWriter, r *http.Request) {

Disabdata = ""
for k:=0; k < 82; k++ {
	arraydata[k] = 'X'
	errordata[k] = ' '
}
	display(w, r, arraydata,errordata,Disabdata,"1", false, "none")
fmt.Fprintf(w, fine,ver)
}

func display(w http.ResponseWriter, r *http.Request, dat byte82, err byte82, dis string, flg string, salvato bool, sug string) {

unonove := [10]string { "X", "1", "2", "3", "4", "5", "6", "7", "8", "9"}
intestaz := ""
if flg == "1" {
	intestaz = "<a class='w3-button w3-border w3-border-blue' href=/listaschemi><b>Scegli uno schema esistente</b></a> oppure<p><a class='w3-button w3-border w3-border-blue' href=/listapartite><b>Riprendi una Partita</b></a> oppure<p><b>Definisci schema in questa pagina</b><p> oppure <a class='w3-button w3-border w3-border-blue' href=/isch><b>Carica nuovo schema nel DB</b></a>"
} else {
	intestaz = "<p><b>Gioca</b>"
}
fmt.Fprintf(w, testa, intestaz)
finito := true
mancanti := 0
errori := 0
for blocco := 0; blocco < 9; blocco++ {
  fmt.Fprintf(w, "<td><table border width=\"100%%\"><tr><th colspan=3>Regione %d</th></tr>",
		blocco+1)
  for incriga := 0; incriga < 3; incriga++ {
    fmt.Fprintf(w, "<tr>")
    for inccol := 0; inccol < 3; inccol++ {
	n := blocco*9+incriga*3+inccol
	classe := "normale"
	if dat[n] == 'X' {
		classe = "tbd"
		finito = false
		mancanti++
	}
	if err[n] == 'E' {
		classe = "errore"
		finito = false
		mancanti++
		errori++
	}
	classe = classe + " w3-large"
	if len(dis) > n && dis[n] == 'D' {
	classe = classe + " w3-lime"
	fmt.Fprintf(w, "<td class='%s' width=\"33%%\"><button type='button' class='w3-button'>%s</button><input type=hidden name=\"d%d\" value=\"%s\"></td>",
	    classe, dat[n:n+1], n, dat[n:n+1])
	} else {
	fmt.Fprintf(w, "<td class='%s'>", classe)
	slct := ""
	for kkk := 0 ; kkk < 10; kkk++ {
	if unonove[kkk] == string(dat[n:n+1]) {
		slct = unonove[kkk]
	}

	}
//magico print
// parametri: ind, default, n.opz.(10)*indice, 2*indice, default
        fmt.Fprintf(w, x2, n, slct, n, n, n, n, n, n, n, n, n, n, n, n, slct)
        fmt.Fprintf(w, "</td>")
    	}
    }
    fmt.Fprintf(w, "</tr>")
  }
  fmt.Fprintf(w, "</table></td>")
  if blocco == 2 || blocco == 5 {
	fmt.Fprintf(w, "</tr><tr>")
  }
}
msg := ""
if finito {
msg = "<b>Partita terminata</b>"
} else {
msg = "Schema non salvato"
if salvato {
 msg = "Schema salvato"
}
}

fmt.Fprintf(w, coda, flg, dis, msg, mancanti, sug)
if errori > 0 {
	fmt.Fprintf(w, "<button class='errore'>Correggi gli errori: %d</div></button>",errori)
}
if flg != "1" {
   fmt.Fprintf(w, coda2)
  if sug != "none" {
  fmt.Fprintf(w, "<script>document.getElementById('sugb').style.display='none'; document.getElementById('expb').style.display=''</script>")
  } else {
  fmt.Fprintf(w, "<script>document.getElementById('sugb').style.display=''; document.getElementById('expb').style.display='none'</script>")
  }
} else {
   fmt.Fprintf(w, coda4)
}
if finito {
	fmt.Fprintf(w, "<script>document.getElementById('slv').style.display='none'; document.getElementById('slv2').style.display='none'</script>")
}
}

func saniti(a string) string {
return strings.Replace(strings.Replace(a,"<","_",-1),"%","_",-1)
}

func got(w http.ResponseWriter, r *http.Request) {
var stringa string = ""
var dsb string = ""
if r.Method != "POST" {
		http.NotFound(w, r)
		return
	}
flg := saniti(r.FormValue("flg"))
salva := saniti(r.FormValue("salva"))
dsb = saniti(r.FormValue("dsb"))
sug := saniti(r.FormValue("sug"))
Disabdata = dsb
for k:=0; k < 81; k++ {
	chiave := fmt.Sprintf("d%d", k)
	valore := saniti(r.FormValue(chiave))
	if len(valore) > 1 {
		errordata[k] = 'E'
		valore = "E"
	}
	if valore == "" {
		valore = "X"
	}
	if flg == "1" {
		if valore != "X" {
		Disabdata = Disabdata + "D"
		} else {
		Disabdata = Disabdata + "X"
		}
	}
	stringa = stringa + valore
}
salvato := false

if salva == "X" {
if flg == "1" {
 if salvaschema(w,r,stringa) != 0 {
	return
 }
 } else {
 if salvapartita(w,r,stringa,Disabdata) == 1 {
	return
 }
 }
 salvato = true
}

for k:=0; k < 81; k++ {
	arraydata[k] = stringa[k]
	if (stringa[k] != 'X') {
		if (stringa[k] < '1' || stringa[k] > '9') {
		errordata[k] = 'E'
		}
		for m:=0; m < 81; m++ {
		if m != k {
		  if stringa[m] == stringa[k] {
		    if (blocco(m) == blocco(k)) ||
		       (riga(m) == riga(k)) ||
		       (colonna(m) == colonna(k)) {
			errordata[k] = 'E'
		    }
		  }
		}
		}
	}
}
display(w, r, arraydata, errordata,Disabdata,"0",salvato,sug)

// Suggerimenti
const ottsugg = 5
var zz Suggerimenti

fmt.Fprintf(w, myfun)
fmt.Fprintf(w, myblu)

//AFIANCO
fmt.Fprintf(w, "</td><td><div id=sugge style='display:%s'>",sug)
fmt.Fprintf(w, "<h3>Suggerimenti ottimizzati (< %d)</h3>", ottsugg)
fmt.Fprintf(w, "<table border><tr><th>Reg.</th><th>Riga</th><th>Col.</th><th>valori</th><th>Commento</th></tr>")
totsugg := 0

for k:=0; k < len(zz); k++ {
  zz[k].Numsugg = 99
}

for k:=0; k < 81; k++ {
if Disabdata[k] != 'D' && arraydata[k] == 'X' {
		 ottim := ""
		 numsugg := 0
		 for c := 49; c < 58; c++ {
		  stampa := true
		for m:=0; m < 81; m++ {
		if m != k {
		  cc := fmt.Sprintf("%d",c)
		  d := fmt.Sprintf("%d",arraydata[m])
		  if cc == d {
		    if (blocco(m) == blocco(k)) ||
		       (riga(m) == riga(k)) ||
		       (colonna(m) == colonna(k)) {
			stampa = false
		    }
		  }
		 }
		}
		  if stampa {
			ottim = ottim + 
			 fmt.Sprintf("<button onclick='myfun(\"d%d\",\"%c\");setta(\"%d\",\"%c\")'>%c</button> ",k,c,k,c,c)
			numsugg++
			zz[k].Numsugg = numsugg
			zz[k].Kappa = k
			zz[k].Ottim = ottim
		  }
		}
// inserito per sort !
		if numsugg == 0 {
			zz[k].Numsugg = numsugg
			zz[k].Kappa = k
			zz[k].Ottim = fmt.Sprintf("<script>myblu(\"d%d\")</script> ",k)
		}
}
}
// sort
zz = miosort(zz)
for iota:=0; iota < len(zz); iota++ {
if zz[iota].Numsugg == 0 {
fmt.Fprintf(w, "<script>document.getElementById('sugge').style.visibility=''</script>")
//	break;
} else {
 if (iota > 0) && (zz[iota-1].Numsugg == 0) {
  break;
 }
}

if zz[iota].Numsugg != 99 {
			numsugg := zz[iota].Numsugg
			k := zz[iota].Kappa
			ottim := zz[iota].Ottim
if Disabdata[k] != 'D' && arraydata[k] == 'X' {
	if numsugg < ottsugg {
		fmt.Fprintf(w, "<tr><td>%d</td><td>%d</td><td>%d</td>",blocco(k)+1, (riga(k) % 3) + 1,
			(colonna(k) % 3) + 1)
		fmt.Fprintf(w, "<td>%s</td>",ottim)
// DEBUG!
//		fmt.Fprintf(w, "<td>%d</td>",numsugg)
// DEBUG!
		if numsugg == 1 {
			fmt.Fprintf(w, "<td class=tbd>determinato!</td>")
		arraydata[k] = ottim[0]
		}
		if numsugg == 0 {
			fmt.Fprintf(w, "<td class=errore>IMPOSSIBILE!</td>")
		}
		if numsugg > 1 {
			fmt.Fprintf(w, "<td>&nbsp;</td>")
		}
		fmt.Fprintf(w, "</tr>")
		totsugg++
	}
}
}

}
fmt.Fprintf(w, "</table><p>")
if totsugg < 81 {
if totsugg > 0 {

	fmt.Fprintf(w, "Per le altre posizioni ")
}
	fmt.Fprintf(w, "non ci sono suggerimenti ottimizzati")
}
//AFIANCO
fmt.Fprintf(w, "</div></td></tr></table>")
fmt.Fprintf(w, fine,ver)
}

const x2 = `
<div class="w3-dropdown-hover">
  <button type='button' onclick='' class="w3-button" id='fx%d'>%s</button>
  <div class="w3-dropdown-content w3-bar-block w3-border-0">
    <input onclick='setta(%d,1)' value=1 class="w3-bar-item w3-button w3-border-0"></input>
    <input onclick='setta(%d,2)' value=2 class="w3-bar-item w3-button w3-border-0"></input>
    <input onclick='setta(%d,3)' value=3 class="w3-bar-item w3-button w3-border-0"></input>
    <input onclick='setta(%d,4)' value=4 class="w3-bar-item w3-button w3-border-0"></input>
    <input onclick='setta(%d,5)' value=5 class="w3-bar-item w3-button w3-border-0"></input>
    <input onclick='setta(%d,6)' value=6 class="w3-bar-item w3-button w3-border-0"></input>
    <input onclick='setta(%d,7)' value=7 class="w3-bar-item w3-button w3-border-0"></input>
    <input onclick='setta(%d,8)' value=8 class="w3-bar-item w3-button w3-border-0"></input>
    <input onclick='setta(%d,9)' value=9 class="w3-bar-item w3-button w3-border-0"></input>
    <input onclick='setta(%d,"X")' value='X' class="w3-bar-item w3-button w3-border-0"></input>
  </div>
</div>
<input type=hidden id='ff%d' name='d%d' value='%s'>
`

const myfun = `
<script>
function myfun(p,q) {
    var x = document.getElementsByName(p);
    var i;
    for (i = 0; i < x.length; i++) {
        x[i].value = q;
        x[i].style.backgroundColor = "00ff33";
    }
return 1
}
</script>`

const myblu = `
<script>
function myblu(p) {
    var x = document.getElementsByName(p);
    var i;
    for (i = 0; i < x.length; i++) {
        x[i].value = "X";
        x[i].style.backgroundColor = "ff0000";
    }
return 1
}
</script>`

func riga(pos int) int {
return ((pos % 9) / 3) + (3 * (blocco(pos) / 3))
}

func colonna(pos int) int {
return ((pos % 9) % 3) + (3 * (blocco(pos) % 3))
}

func blocco(pos int) int {
return (pos / 9)
}

func listaschemi(w http.ResponseWriter, r *http.Request) {
var zz []string
zz = nil
qry := "select vd from schema order by vd"
rows, err := globaldb.Query(qry)
if err != nil {
	fmt.Fprintf(w, "query schema err. %v\n", err)
	return
}
for rows.Next() {
	z := ""
	rows.Scan(&z)
	zz = append(zz, z)
}
rows.Close()
for k:=0 ; k < len(zz); k++ {
display2(w,r,zz[k],k+1,"Schema")
fmt.Fprintf(w, "<form action=/gotdata method=post>")
fmt.Fprintf(w, "<input type=hidden name=flg value=1>")
disa := ""
str := zz[k]
for j:=0; j < len(str); j++ {
 if str[j] != 'X' {
  disa = disa + "D"
  fmt.Fprintf(w, "<input type=hidden name=d%d value=%c>", j, str[j])
 } else {
  disa = disa + "X"
 }
}
fmt.Fprintf(w, "<button class='w3-button w3-blue' type=submit>Gioca con lo Schema %d</button></form><hr>",k+1)
}
if len(zz) == 0 {
fmt.Fprintf(w, "<html><body><h2>GO!Giudoku</h2><h2>Non ci sono Schemi salvati</h2>")
fmt.Fprintf(w, "<a href='javascript:history.go(-1)'>Back</a> <a href=/help>Help!</a>")
}
fmt.Fprintf(w, fine,ver)
}

func listapartite(w http.ResponseWriter, r *http.Request) {
// modificata per togliere il link a schema 24-gen-17
// ver 1.8

var zz []Partita
zz = nil
qry := "select vd, ds, oo from partita order by oo desc"
rows, err := globaldb.Query(qry)
if err != nil {
	fmt.Fprintf(w, "err query partita %v\n", err)
	return
}
for rows.Next() {
	var z Partita
	rows.Scan(&z.Vd, &z.Ds, &z.Oo)
	zz = append(zz, z)
}
rows.Close()
for k:=0 ; k < len(zz); k++ {
display2(w,r,zz[k].Vd,k,"Partita")
fmt.Fprintf(w, "<form action=/gotdata method=post>")
fmt.Fprintf(w, "<input type=hidden name=flg value=0>")
disa := ""
str := zz[k].Vd
dsx := zz[k].Ds
for j:=0; j < len(str); j++ {
 fmt.Fprintf(w, "<input type=hidden name=d%d value=%c>", j, str[j])
 if dsx[j] == 'D' {
  disa = disa + "D"
 } else {
  disa = disa + "X"
 }
}
 fmt.Fprintf(w, "<input type=hidden name=dsb value=%s>",disa)
o := "(n/a)"
if zz[k].Oo != "" {
	o = zz[k].Oo
}
fmt.Fprintf(w, "<button class='w3-button w3-blue' type=submit>Riprendi la Partita %d</button> del %s</form><hr>",k,o)
}
if len(zz) == 0 {
fmt.Fprintf(w, "<html><head><link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'></head><body><div class='w3-container'><h2>GO!Giudoku</h2><h2>Non ci sono Partite salvate</h2>")
fmt.Fprintf(w, "<a class='w3-button w3-border w3-border-blue' href='javascript:history.go(-1)'>Back</a> <a class='w3-button w3-border w3-border-blue' href=/help>Help!</a>")
}
fmt.Fprintf(w, fine,ver)
}

func display2(w http.ResponseWriter, r *http.Request, dat string, kk int,tt string) {

intestaz := fmt.Sprintf("<h2>%s %d</h2>",tt,kk)

fmt.Fprintf(w, testa2, intestaz)
for blocco := 0; blocco < 9; blocco++ {
  fmt.Fprintf(w, "<td><table border width=\"100%%\">")
  for incriga := 0; incriga < 3; incriga++ {
    fmt.Fprintf(w, "<tr>")
    for inccol := 0; inccol < 3; inccol++ {
	n := blocco*9+incriga*3+inccol
	classe := "normale"
	if dat[n] == 'X' {
		classe = "tbd"
	}
	classe = classe + " w3-large"
	fmt.Fprintf(w, "<td class='%s'>&nbsp;<br>%s<br>&nbsp;</td>",
	    classe, dat[n:n+1])
    }
    fmt.Fprintf(w, "</tr>")
  }
  fmt.Fprintf(w, "</table></td>")
  if blocco == 2 || blocco == 5 {
	fmt.Fprintf(w, "</tr><tr>")
  }
}

fmt.Fprintf(w, coda3)
}

func salvaschema(w http.ResponseWriter, rr *http.Request, valori string) int {
qry := fmt.Sprintf("insert into schema (vd) values ('%s')", valori)
_, err5 := globaldb.Exec(qry)
if err5 != nil {
	fmt.Fprintf(w, "err5=%v", err5)
	return 1
}
return 0
}

func checkpartita(vv string) string {
oo := ""
qry := fmt.Sprintf("select oo from partita where vd = '%s'", vv)
rows, err6 := globaldb.Query(qry)
        if err6 != nil {
                return "/"
        }
n := 0
for rows.Next() {
	rows.Scan(&oo)
	n++
}
rows.Close()
if n == 0 {
 return ""
}
return oo
}

func salvapartita(w http.ResponseWriter, rr *http.Request, valori string, dis string) int {

const layout = "2006-01-02 15:04:05"

yy := checkpartita(valori)
if yy != "" {
	fmt.Fprintf(w,"<html><script>alert('Partita presente in archivio: %s')</script></html>",yy)
	return 2
}
o := fmt.Sprintf("%s",time.Now().Format(layout))
s := Partita{valori,dis,o}
qry := fmt.Sprintf("insert into partita (vd, ds, oo) values ('%s', '%s', '%s')", s.Vd, s.Ds, s.Oo)
_, err51 := globaldb.Exec(qry)
if err51 != nil {
	fmt.Fprintf(w, "err51=%v", err51)
	return 1
}
return 0
}

func help(w http.ResponseWriter, r *http.Request) {
fmt.Fprintf(w, formHelp, ver)
}

func vers(w http.ResponseWriter, r *http.Request) {
fmt.Fprintf(w, formVer, chglog)
}

func isch(w http.ResponseWriter, r *http.Request) {

var seq = []int {0,1,2,9,10,11,18,19,20,3,4,5,12,13,14,21,22,23,6,7,8,15,16,17,24,25,26,27,28,29,36,37,38,45,46,47,30,31,32,39,40,41,48,49,50,33,34,35,42,43,44,51,52,53,54,55,56,63,64,65,72,73,74,57,58,59,66,67,68,75,76,77,60,61,62,69,70,71,78,79,80}
out := ""
sc := r.FormValue("sc")
if sc != "" {
	fmt.Fprintf(w, formisch2)
	if len(sc) < 81 {
		fmt.Fprintf(w, "<p class='w3-button w3-red'>ERRORE LUNGHEZZA: %d</p>", len(sc))
		fmt.Fprintf(w, formisch3)
		return
	}
	sc = strings.Replace(sc, "\r\n", "", -1)
	fmt.Fprintf(w, "<p>ricevuti dati:\n=%s=\nlen=%d", sc, len(sc))
	for k := 0; k < len(seq); k++ {
		i := seq[k]
		j := i + 1
		if i < 80 {
			out = out + sc[i:j]
		} else {
			out = out + sc[i:]
		}
	}
	fmt.Fprintf(w, "<p>out=%s. len=%d", out, len(out))
	out = strings.Replace(out, "0", "X", -1)
	qry := fmt.Sprintf("insert into schema (vd) values ('%s')", out)
	_, err := globaldb.Exec(qry)
	if err != nil {
		fmt.Fprintf(w, "<p class='w3-button w3-red'>err insert: %v</p>", err)
	} else {
		fmt.Fprintf(w, "<p class='w3-button w3-green'>insert ok</p>")
	}
	fmt.Fprintf(w, formisch3)
	return
}
fmt.Fprintf(w, formisch)
}

const formisch2 = `
<html><head><title>GO!Giudoku</title>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head><body><div class='w3-container'>
<h1>Import Schema - Esito</h1>
`

const formisch3 = `
<p><a class='w3-button w3-border w3-border-blue' href=/>Torna a Home</a>
</div></body></html>
`

const formisch = `
<html><head><title>GO!Giudoku</title>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head><body><div class='w3-container'>
<h1>Import Schema</h1>
<form method=post action=/isch>
<textarea name=sc rows=9 cols=10></textarea>
<button type=submit>Invia</button>
<a class='w3-button w3-border w3-border-blue' href=/>Torna a Home</a>
</div></body></html>
`

const formHelp = `
<html><head><title>GO!Giudoku</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>.gray { background-color: lime }
.yel { background-color: orange }
.red { background-color: red }
</style></head>
<body><div class='w3-container'>
<h2>GO!Giudoku</h2>
<h2>Help</h2>
Questo programma (vers. %s) permette di giocare a <b>Sudoku</b>.
<p>Il programma segnala gli <b>errori</b> (secondo le regole del Sudoku)<br>
e fornisce <b>suggerimenti</b> per la prossima mossa.
<p>Nella prima fase:
<ul>
<li>si introduce lo schema, oppure
<li>si sceglie uno schema in archivio, oppure
<li>si riprende una partita salvata precedentemente,
</ul>
successivamente si gioca.
<p>Ogni casella <span class=gray>verde</span> fa parte dello schema iniziale<br>
e <b>non pu&ograve; essere modificata</b> durante il gioco.
<p>Le caselle <span class=yel>gialle</span> vanno riempite con un numero da 1 a 9.
<p>Le caselle <span class=red>rosse</span> sono in conflitto fra loro (secondo le regole del Sudoku) e vanno modificate.
<p><a class='w3-button w3-border w3-border-blue' href="javascript:history.go(-1)">back</a>
<h2>Privacy e Cookie Policy</h2>
Questo sito non utilizza "cookie"<br>
</div></body></html>
`

const testa = `
<html><head><title>GO!Giudoku</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
th { font-family: sans-serif }
td, tr, input, .normale { text-align: center; vertical-align: middle; }
pre { font-size: 120%% }
small { font-size: 60%% }
table { font-family: monospace; border-width: 3; border-color: blue; border-collapse: collapse; }
.errore { color: white; background-color: red; text-align: center; vertical-align: middle; }
.tbd { background-color: orange; text-align: center; vertical-align: middle; }
.chs { background-color: BBBBBB; text-align: center; vertical-align: middle; }
.hid{ visibility: hidden }
#em { color: red; }
.w3-bar-item {width: 45px!important; padding:3px 10px 3px 10px!important}
.top {vertical-align: top !important; text-align: left !important; }
.uno {font-family: arial, sans !important; }
</style>
<script>
function setta(n,x) {
nfx = "fx" + n
nff = "ff" + n
document.getElementById(nff).value = x
document.getElementById(nfx).innerHTML = x
document.getElementById(nfx).style.backgroundColor = "00ff33";
return 1
}
</script>
</head>
<body><div class='w3-container'><table class='uno' width='80%%'><tr class='top'><td class='top'><blockquote><h2>GO!Giudoku</h2>
%s
<p><a class='w3-button w3-border w3-border-blue' href="javascript:history.go(-1)">Back</a> 
 <a class='w3-button w3-border w3-border-blue' href=/help>Help!</a>
 <a class='w3-button w3-border w3-border-blue' href=/>Home</a><p>
<form action=/gotdata method=post>
<table width='100%%'><tr>
`

const testa2 = `
<html><head><title>GO!Giudoku</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
th { font-family: sans-serif }
td, tr, input, .normale { text-align: center; vertical-align: middle; }
pre { font-size: 120%% }
small { font-size: 60%% }
table { font-family: monospace; border-width: 3; border-color: blue; border-collapse: collapse; }
.errore { color: white; background-color: red; text-align: center; vertical-align: middle; }
.tbd { background-color: orange; text-align: center; vertical-align: middle; }
.chs { background-color: BBBBBB; text-align: center; vertical-align: middle; }
.hid{ visibility: hidden }
#em { color: red; }
.w3-bar-item {width: 45px!important; padding:3px 10px 3px 10px!important}
.top {vertical-align: top !important; text-align: left !important; }
.uno {font-family: arial, sans !important; }
</style>
<script>
function setta(n,x) {
nfx = "fx" + n
nff = "ff" + n
document.getElementById(nff).value = x
document.getElementById(nfx).innerHTML = x
document.getElementById(nfx).style.backgroundColor = "00ff33";
return 1
}
</script>
</head>
<body><div class='w3-container'><blockquote><h2>GO!Giudoku</h2>
%s
<p><a class='w3-button w3-border w3-border-blue' href="javascript:history.go(-1)">Back</a> 
 <a class='w3-button w3-border w3-border-blue' href=/help>Help!</a>
 <a class='w3-button w3-border w3-border-blue' href=/>Home</a><p>
<form action=/gotdata method=post>
<table width='75%%'><tr>
`

const coda = `
</tr></table><p>
<input type=hidden name=flg value=%s></input>
<input type=hidden name=dsb value=%s></input>
<span id=slv>
<input type='checkbox' name='salva' value='X'>Salva</input>
</span>
&nbsp;&nbsp; %s &nbsp;&nbsp;<button class='w3-button w3-border w3-border-blue' type=submit>Ok</button>
Posizioni mancanti: %d
<input type="hidden" name="sug" id="sug" value="%s">
</form>
`

const coda3 = `
</tr></table><p>
</form>
</blockquote>
`

const coda2 = `
<span id=slv2>
<span id=sugb>
Expert mode attivo.
<input type="button" class='w3-button w3-border w3-border-blue' value="Attiva Suggerimenti" onclick="document.getElementById('sugge').style.visibility=''; document.getElementById('sug').value='normal'; document.getElementById('sugb').style.display='none'; document.getElementById('expb').style.display=''">
</span>
<span id=expb>
Modo suggerimento attivo.
<input type="button" class='w3-button w3-border w3-border-blue' value="Attiva Expert mode" title="Disabilita i suggerimenti. Pu&ograve; essere cambiato ad ogni mossa" onclick="document.getElementById('sugge').style.visibility='hidden'; document.getElementById('sug').value='none'; document.getElementById('expb').style.display='none'; document.getElementById('sugb').style.display=''">
</span>
</span>
`

const coda4 = `
<span title="Disabilita i suggerimenti. Pu&ograve; essere cambiato ad ogni mossa" id="em">Expert mode attivo. Pu&ograve; essere cambiato ad ogni mossa</span>
<script>document.getElementById('sug').value='normal'</script>
<script>document.getElementById('em').style.display='none'</script>
<input type="button" class='w3-button w3-border w3-border-blue' id="btnexp" title="Disabilita i suggerimenti. Pu&ograve; essere cambiato ad ogni mossa" value="Attiva Expert mode" onclick="document.getElementById('sug').value='none'; document.getElementById('em').style.display='inline'; document.getElementById('btnexp').style.display='none'">
`

const fine = `
</blockquote>
<p><hr>
<div class='w3-container'>
<div class='w3-button w3-border w3-round'>Grazie per aver usato questo programma (vers. %s) <a class='w3-button w3-border w3-border-orange w3-hover-blue' href="/vers">Changelog</a>.
</div>
</div>
</div></body>
</html>`

const formVer = `
<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body><div class='w3-container'>
<h2>GO!Giudoku</h2>
<h2>Changelog</h2>
<a class='w3-button w3-border w3-border-orange' href='javascript:history.go(-1)'>back</a><p>
<div class='w3-card w3-hoverable' style='width:40%%'>
%s
</div>
</div>
</body></html>
`
