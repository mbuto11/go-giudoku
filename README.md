# go-giudoku

just another Sudoku playground

1. Create a project dir
1. Download or clone: *main.go*, *app.go* and *sudoku-dbinit.sql*
1. go build -o *program*
1. copy the **sqlite3** db named *sudoku.db* from dir *setup* into your dir
1. execute the *program*

then, using your browser, connect to http://localhost:9000/

Enjoy!

**Note**: *all human interface is in italian! - Translations welcome!*
If you want to import more schemes, use the file *easy50.txt* in *setup* dir
